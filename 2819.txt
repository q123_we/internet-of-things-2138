public class Main {
	public static void main(String[] args) {
		// write your code here
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();

		for (int i = 2; i < n; i++) {
			if (n%i == 0) {
				System.out.println(n+" is not a prime number");
				return;
			}
		}
		System.out.println(n+ " is a prime number");
		
	}
}